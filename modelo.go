package main

import (
	"gopkg.in/mgo.v2/bson"
)

type Modelo struct {
	//imprime el ID
	ID     bson.ObjectId `bson:"_id,omitempty" json:"_id,omitempty"`
	Id     int           `json:"id"`
	Nombre string        `json:"nombre"`
}

type Modelos []Modelo
