package main

import (
	"net/http"

	"github.com/gorilla/mux"
)

type Route struct {
	Name       string
	Method     string
	Pattern    string
	HandleFunc http.HandlerFunc
}

type Routes []Route

func NewRouter() *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	for _, route := range routes {
		router.
			Name(route.Name).
			Methods(route.Method).
			Path(route.Pattern).
			Handler(route.HandleFunc)
	}
	return router
}

var routes = Routes{
	Route{
		"Index",
		"GET",
		"/",
		Index,
	}, Route{
		"Contacto",
		"GET",
		"/contacto",
		Contacto,
	},
	Route{
		"Producto",
		"GET",
		"/producto/{id}",
		Producto,
	},
	Route{
		"ProductoLista",
		"GET",
		"/productos",
		ProductoLista,
	},
	Route{
		"CrearProducto",
		"POST",
		"/producto",
		CrearProducto,
	},
	Route{
		"Actualizar",
		"PUT",
		"/producto/{id}",
		Actualizar,
	},
}
