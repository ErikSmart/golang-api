package main

import (
	"log"
	"net/http"
)

func main() {
	router := NewRouter()
	/* http.HandleFunc("/",func (w http.ResponseWriter, r *http.Request)  {
		fmt.Fprintf(w,"Hola mundo con Go")
	}) */
	server := http.ListenAndServe(":8080", router)
	log.Fatal(server)
}
