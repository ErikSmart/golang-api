package main

import (
	"encoding/json"
	"fmt"

	"github.com/gorilla/mux"

	// Descargado de https://gopkg.in/
	"net/http"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// Funcion para inicializar Mongo
func getSeccion() *mgo.Session {
	session, err := mgo.Dial("mongodb://172.18.0.2")
	if err != nil {
		panic(err)
	}
	return session
}

// Variable con el nombre de la DB, La coleccion
var coleccion = getSeccion().DB("elgo").C("productos")

/* var productos = Modelos{
	Modelo{1, "Peluche Totoro"},
	Modelo{2, "Peluche Kirby"},
	Modelo{3, "Peluche Kirbys"},
} */

func response(w http.ResponseWriter, status int, resultado Modelo) {
	w.Header().Set("Content-type", "application/json")
	w.WriteHeader(status)
	json.NewEncoder(w).Encode(resultado)
}

func Index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hola mundo con Go")
}
func Contacto(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Contacto")
}
func Producto(w http.ResponseWriter, r *http.Request) {
	//necesario para inicializar el parametro
	params := mux.Vars(r)
	id := params["id"]
	// Se requiere que este instalado e importado "gopkg.in/mgo.v2/bson" para traformar en Boson
	// Si es diferente de un valor Bson dara 404
	if !bson.IsObjectIdHex(id) {
		w.WriteHeader(404)
		return
	}
	//El objeto id de mongodb
	oid := bson.ObjectIdHex(id)
	//Se dirige al modelo
	elproducto := Modelo{}
	// FindId encuentra el ID en MongoDB si Pongo solo Find retorna error
	err := coleccion.FindId(oid).One(&elproducto)
	if err != nil {
		w.WriteHeader(404)
		return
	}

	/* w.Header().Set("Content-type", "application/json")
	w.WriteHeader(200)
	json.NewEncoder(w).Encode(elproducto) */
	//fmt.Fprintf(w, "Lo que hay en el parametro es: %s", id)
	//Obtenido de la funcion response las cabezeras
	response(w, 200, elproducto)
}

func ProductoLista(w http.ResponseWriter, r *http.Request) {
	//Obtiene el modelo de modelo.go
	var listarProducto []Modelo
	//Busca en Mongo y con Sort("-_id") ordena por el ultimo creado
	err := coleccion.Find(bson.M{}).Sort("-_id").All(&listarProducto)

	if err != nil {
		w.WriteHeader(404)
		panic(err)
	} else {
		//fmt.Print(listarProducto)
	}
	//Esta es la salida que obtiene en el request
	json.NewEncoder(w).Encode(listarProducto)

}

func CrearProducto(w http.ResponseWriter, r *http.Request) {
	decodificar := json.NewDecoder(r.Body)
	var productos_datos Modelo
	err := decodificar.Decode(&productos_datos)
	if err != nil {
		w.WriteHeader(404)
		panic(err)
	} else {
		fmt.Print(productos_datos)
	}
	defer r.Body.Close()
	//productos = append(productos, productos_datos)
	err = coleccion.Insert(productos_datos)
	// Esta condicion es obligatoria
	if err != nil {
		w.WriteHeader(500)
		return
	}

	/* w.Header().Set("Content-type", "application/json")
	w.WriteHeader(200)
	json.NewEncoder(w).Encode(productos_datos) */
	response(w, 200, productos_datos)
}

func Actualizar(w http.ResponseWriter, r *http.Request) {
	//necesario para inicializar el parametro
	params := mux.Vars(r)
	id := params["id"]
	// Se requiere que este instalado e importado "gopkg.in/mgo.v2/bson" para traformar en Boson
	// Si es diferente de un valor Bson dara 404
	if !bson.IsObjectIdHex(id) {
		w.WriteHeader(404)
		return
	}
	//El objeto id de mongodb
	oid := bson.ObjectIdHex(id)

	//Decodificar cuerpo
	decodificar := json.NewDecoder(r.Body)

	//Se dirige al modelo
	elproducto := Modelo{}
	// FindId encuentra el ID en MongoDB si Pongo solo Find retorna error
	err := decodificar.Decode(&elproducto)
	if err != nil {
		w.WriteHeader(404)
		return
	}
	// cierra la conexion obio
	defer r.Body.Close()

	documento := bson.M{"_id": oid}
	cambio := bson.M{"$set": elproducto}
	err = coleccion.Update(documento, cambio)
	fmt.Print(documento)
	if err != nil {
		w.WriteHeader(404)
		return
	}
	//Obtenido de la funcion response las cabezeras
	response(w, 200, elproducto)
	json.NewEncoder(w).Encode(documento)
}
